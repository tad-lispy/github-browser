describe("GitHub Browser org page", () => {
  beforeEach(() => {
    cy.visit("http://localhost:5000/elm-community")
  })
  
  it("displays org name in the nav bar", () => {
    cy.get(".navbar")
      .contains("elm-community")
  })

  it("displays many cards with repositories", () => {
    cy.get(".card")
      .then((cards) => {
        expect(cards.length).to.be.above(10)
        expect(cards)
          .to.all.satisfy((card) => {
            return card.text().match(/elm-community\/.+/)
          }, "The card doesn't display a title as expected")
          .and.to.all.satisfy((card) => {
            return card.text().match(/\d+ stars/)
          }, "The card doesn't display a star counter as expected")

          .and.to.all.satisfy((card) => {
            return card.text().match(/\d+ forks/)
          }, "The card doesn't display a forks counter as expected")
      })
  })

  describe("repo card", () => {
    it("has a button to display list of branches", () => {
      cy.get(".card:first .btn.btn-primary")
        .click()
        .then(() => {
          cy.location("pathname")
        })
        .then((pathname) => {
          expect(pathname).to.match(/\/elm-community\/.+/)
        })
    })
  })
}) 
