describe("GitHub Browser start page", () => {
  beforeEach(() => {
    cy.visit("http://localhost:5000/")
  })
  
  it("contains an input", () => {
    cy.get("input")
  })

  it("contains a find button", () => {
    cy.contains("button", "Find")
  })

  it("can be submitted by clicking on the button", () => {
    cy.get("input")
      .type("elm-community")
    
    cy.get("button")
      .click()
    
    cy.location("pathname")
      .then((pathname) => {
        expect(pathname).to.equal("/elm-community")
      })
  })
  
  it("can be submitted by pressing enter on the input", () => {
    cy.get("input")
      .type("elm-community{enter}")
    
    cy.location("pathname")
      .then((pathname) => {
        expect(pathname).to.equal("/elm-community")
      })
  })
}) 
